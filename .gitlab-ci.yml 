# Define different stages of the CI/CD pipeline
stages:
  - build
  - test
  - deploy


# Build stage configuration
build:
  stage: build
  script:
    # Set PowerShell error action preference to "Stop" for better error handling
    - 'if($ErrorActionPreference -ne "Stop") { $ErrorActionPreference = "Stop" }'
    # Try to remove Docker container 'quasar-app'; catch and print any errors
    - 'try { docker rm quasar-app -f } catch { Write-Host "Error: $_" }'
    # Try to remove Docker image 'quasar-app:latest'; catch and print any errors
    - 'try { docker rmi quasar-app:latest -f } catch { Write-Host "Error: $_" }'
    # Display a message indicating cleaning up project directory and file-based variables
    - 'Write-Host "Cleaning up project directory and file-based variables"'
    # Build Docker image from Dockerfile and tag it as 'quasar-app:latest'
    - docker build -f ./Dockerfile -t quasar-app:latest .
    # Run the Docker container named 'quasar-app' in detached mode, mapping port 8080 to 80
    - docker run --name quasar-app -d -p 8080:80 quasar-app
  tags:
    - quasar-app-v1.0 # Specify the tag for this job


# Test stage configuration
test:
  stage: test
  script:
    # Add your test commands here
    - echo "Running tests..."
  tags:
    - quasar-app-v1.0 # Specify the tag for this job


# Deploy stage configuration
deploy:
  stage: deploy
  script:
    # Add your deployment commands here
    - echo "Deploying to production..."
    # Example: You might deploy to a Kubernetes cluster, AWS, or any other platform
  tags:
    - quasar-app-v1.0 # Specify the tag for this job


# Add other configurations, such as variables, before_script, after_script, etc.
